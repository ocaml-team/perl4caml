(* Thoroughly test HV-related functions.
 * $Id: 120-hv.ml,v 1.1 2005/01/28 23:09:33 rich Exp $
 *)

open Perl

let () =
  let hv = hv_empty () in
  hv_set hv "foo" (sv_of_int 1);
  hv_set hv "bar" (sv_of_int 2);
  hv_set hv "foo" (sv_of_int 42);
  assert (42 = int_of_sv (hv_get hv "foo"));
  assert (2 = int_of_sv (hv_get hv "bar"));
  assert (hv_exists hv "foo");
  assert (not (hv_exists hv "baz"));
  hv_clear hv;
  assert (not (hv_exists hv "foo"));
  assert (not (hv_exists hv "bar"));

  ignore (eval "%h = ( foo => 1, bar => 2 )");
  let hv = get_hv "h" in
  assert (1 = int_of_sv (hv_get hv "foo"));
  assert (2 = int_of_sv (hv_get hv "bar"));
  assert (not (hv_exists hv "baz"));

;;
Gc.full_major ()
