(* Thoroughly test AV-related functions.
 * $Id: 110-av.ml,v 1.1 2005/01/28 23:09:33 rich Exp $
 *)

open Perl

let () =
  let av = av_empty () in
  assert ([] = list_of_av av);
  av_push av (sv_of_int 42);
  av_push av (sv_of_int 84);
  av_unshift av (sv_of_int 21);
  av_set av 0 (sv_of_int 11);
  assert (3 = av_length av);
  assert (84 = int_of_sv (av_pop av));
  assert (2 = av_length av);
  assert (11 = int_of_sv (av_shift av));
  assert (1 = av_length av);
  assert (42 = int_of_sv (av_pop av));
  av_extend av 3;
  av_set av 0 (sv_of_int 11);
  av_set av 1 (sv_of_int 22);
  av_set av 2 (sv_of_int 33);
  av_set av 3 (sv_of_int 44);
  assert (4 = av_length av);
  assert (33 = int_of_sv (av_get av 2));
  assert (22 = int_of_sv (av_get av 1));
  assert (44 = int_of_sv (av_pop av));
  assert (3 = av_length av);
  assert (33 = int_of_sv (av_pop av));
  assert (11 = int_of_sv (av_shift av));
  assert (22 = int_of_sv (av_pop av));
  assert ([] = list_of_av av);

  ignore (eval "@a = ( 'foo', 'bar' )");
  let av = get_av "a" in
  assert ("foo" = string_of_sv (av_get av 0));
  assert ("bar" = string_of_sv (av_get av 1));
;;

Gc.full_major ()
