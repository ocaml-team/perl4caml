(* Basic subroutine call.
 * $Id: 030-call.ml,v 1.1 2005/01/28 23:09:33 rich Exp $
 *)

open Perl

let () =
  ignore (eval "sub test { 42 + $_[0] }");
  let sv = call ~fn:"test" [sv_of_int 10] in
  assert (52 = int_of_sv sv);;

Gc.full_major ()
