(** Wrapper around Perl [Net::Google::Search] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_Net_Google_Search.ml,v 1.4 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

open Pl_Net_Google_Response

let _ = eval "use Net::Google::Search"

class net_google_search sv =

object (self)

  method key =
    string_of_sv (call_method sv "key" [])
  method set_key v =
    call_method_void sv "key" [sv_of_string v]
  method query =
    string_of_sv (call_method sv "query" [])
  method set_query v =
    call_method_void sv "query" [sv_of_string v]
  method starts_at =
    int_of_sv (call_method sv "starts_at" [])
  method set_starts_at v =
    call_method_void sv "starts_at" [sv_of_int v]
  method max_results =
    int_of_sv (call_method sv "max_results" [])
  method set_max_results v =
    call_method_void sv "max_results" [sv_of_int v]
  method restrict types =
    string_of_sv (call_method sv "restrict" (List.map sv_of_string types))
  method filter =
    bool_of_sv (call_method sv "filter" [])
  method set_filter v =
    call_method_void sv "filter" [sv_of_bool v]
  method safe =
    bool_of_sv (call_method sv "safe" [])
  method set_safe v =
    call_method_void sv "safe" [sv_of_bool v]
  method lr langs =
    string_of_sv (call_method sv "lr" (List.map sv_of_string langs))
  method ie encs =
    string_of_sv (call_method sv "ie" (List.map sv_of_string encs))
  method oe encs =
    string_of_sv (call_method sv "oe" (List.map sv_of_string encs))
  method return_estimatedTotal =
    bool_of_sv (call_method sv "return_estimatedTotal" [])
  method set_return_estimatedTotal v =
    call_method_void sv "return_estimatedTotal" [sv_of_bool v]
  method response =
    let sv = call_method sv "response" [] in
    let av = deref_array sv in
    av_map (fun sv -> new net_google_response sv) av
  method results =
    let sv = call_method sv "results" [] in
    let av = deref_array sv in
    av_map (fun sv -> new net_google_response sv) av

end

(* let new_ = ... *)
