(** Wrapper around Perl [Data::Dumper] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_Data_Dumper.ml,v 1.3 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

let _ = eval "use Data::Dumper"

class data_dumper (sv : sv) =

object (self)
  method sv = sv



end

let new_ r =
  let sv = call_class_method "Data::Dumper" "new" [r] in
  new data_dumper sv

let dump r =
  string_of_sv (call_class_method "Data::Dumper" "Dump" [r])

let dump_hv hv =
  dump (arrayref (av_of_sv_list [hashref hv]))

let dump_av av =
  dump (arrayref av)
