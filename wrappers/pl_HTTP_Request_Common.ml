(** Wrapper around Perl [HTTP::Request::Common] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_HTTP_Request_Common.ml,v 1.2 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

open Pl_HTTP_Request

let _ = eval "use HTTP::Request::Common"

let get, head, put =
  let dofn fn url headers =
    let args =
      sv_of_string url ::
	List.fold_right (fun (k, v) rest ->
			   sv_of_string k :: sv_of_string v :: rest)
	headers [] in
    new http_request (call ~fn args)
  in
  let get = dofn "GET" in
  let head = dofn "HEAD" in
  let put = dofn "PUT" in
  get, head, put

let post url ?form headers =
  let hv = hv_empty () in
  (match form with
     | None -> ()
     | Some xs -> List.iter (fun (k, v) -> hv_set hv k (sv_of_string v)) xs);
  let args =
    sv_of_string url :: hashref hv ::
      List.fold_right (fun (k, v) rest ->
			 sv_of_string k :: sv_of_string v :: rest)
      headers [] in
  new http_request (call ~fn:"POST" args)
