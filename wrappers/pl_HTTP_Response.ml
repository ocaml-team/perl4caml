(** Wrapper around Perl [HTTP::Response] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_HTTP_Response.ml,v 1.5 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

open Pl_HTTP_Message
open Pl_HTTP_Request

let _ = eval "use HTTP::Response"

class http_response sv =

object (self)
  inherit http_message sv

  method code =
    string_of_sv (call_method sv "code" [])
  method set_code code =
    call_method_void sv "code" [sv_of_string code]
  method message =
    string_of_sv (call_method sv "message" [])
  method set_message message =
    call_method_void sv "message" [sv_of_string message]
  method request =
    new http_request (call_method sv "request" [])
  method set_request (req : http_request) =
    call_method_void sv "request" [req#sv]
  method status_line =
    string_of_sv (call_method sv "status_line" [])
  method base =
    string_of_sv (call_method sv "base" [])
  method as_string =
    string_of_sv (call_method sv "as_string" [])
  method is_info =
    bool_of_sv (call_method sv "is_info" [])
  method is_success =
    bool_of_sv (call_method sv "is_success" [])
  method is_redirect =
    bool_of_sv (call_method sv "is_redirect" [])
  method is_error =
    bool_of_sv (call_method sv "is_error" [])
  method error_as_HTML =
    string_of_sv (call_method sv "error_as_HTML" [])

end

(* let new_ ... *)
