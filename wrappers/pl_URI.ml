(** Wrapper around Perl [URI] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_URI.ml,v 1.6 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

let _ = eval "use URI"

class uri sv =

object (self)

  method sv = sv

  method scheme =
    string_of_sv (call_method sv "scheme" [])
  method set_scheme scheme =
    call_method_void sv "scheme" [sv_of_string scheme]
  method opaque =
    string_of_sv (call_method sv "opaque" [])
  method set_opaque opaque =
    call_method_void sv "opaque" [sv_of_string opaque]
  method path =
    string_of_sv (call_method sv "path" [])
  method set_path path =
    call_method_void sv "path" [sv_of_string path]
  method fragment =
    string_of_sv (call_method sv "fragment" [])
  method set_fragment fragment =
    call_method_void sv "fragment" [sv_of_string fragment]
  method set_no_fragment () =
    call_method_void sv "fragment" [sv_undef ()]
  method as_string =
    string_of_sv (call_method sv "as_string" [])
  method canonical =
    string_of_sv (call_method sv "canonical" [])
  method abs base =
    string_of_sv (call_method sv "abs" [sv_of_string base])
  method rel base =
    string_of_sv (call_method sv "rel" [sv_of_string base])


  method host =
    string_of_sv (call_method sv "host" [])
  method set_host host =
    call_method_void sv "host" [sv_of_string host]
  method port =
    string_of_sv (call_method sv "port" [])
  method set_port port =
    call_method_void sv "port" [sv_of_string port]
  method host_port =
    string_of_sv (call_method sv "host_port" [])
  method set_host_port host_port =
    call_method_void sv "host_port" [sv_of_string host_port]
  method default_port =
    int_of_sv (call_method sv "default_port" [])

end

let new_ ?scheme str =
  let args =
    [sv_of_string str] @
    match scheme with
	None -> []
      | Some scheme -> [sv_of_string scheme] in
  let sv = call_class_method "URI" "new" args in
  new uri sv

let new_abs str base =
  let sv = call_class_method "URI" "new_abs" [sv_of_string str;
					      sv_of_string base] in
  new uri sv
