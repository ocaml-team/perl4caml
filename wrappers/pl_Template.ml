(** Wrapper around Perl [Template] class (Template Toolkit). *)
(*  Copyright (C) 2003 Dave Benjamin <dave@3dex.com>.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_Template.ml,v 1.3 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

let _ = eval "use Template"

exception Not_implemented
exception Error of string

module Variant =

struct

  type t =
    | Null
    | String of string
    | Int of int
    | Float of float
    | Bool of bool
    | Array of t list
    | Hash of (string * t) list;;

end

let rec sv_of_variant = function
  | Variant.Null -> sv_undef ()
  | Variant.String s -> sv_of_string s
  | Variant.Int i -> sv_of_int i
  | Variant.Float f -> sv_of_float f
  | Variant.Bool b -> sv_of_bool b
  | Variant.Array xs -> arrayref (av_of_sv_list (List.map sv_of_variant xs))
  | Variant.Hash xs -> hashref
      (let hv = hv_empty () in
         List.iter (fun (k, v) -> hv_set hv k (sv_of_variant v)) xs;
         hv
      );;

let hv_of_string_pair_list pairs =
  let hv = hv_empty () in
    List.iter (fun (k, v) -> hv_set hv k (sv_of_string v)) pairs;
    hv

class template sv =

object (self)

  method process file vars =
    let output = sv_of_string "" in
    let args = [sv_of_string file; sv_of_variant vars; scalarref output] in
    let result = call_method sv "process" args in
      if not (sv_is_true result) then
        raise (Error self#error)
      else
        string_of_sv output

  method error =
    string_of_sv (call_method sv "error" [])

end

let may f = function None -> () | Some v -> f v

let new_ ?start_tag ?end_tag ?tag_style ?pre_chomp ?post_chomp ?trim
  ?interpolate ?anycase ?include_path ?delimiter ?absolute ?relative
  ?default ?blocks ?auto_reset ?recursion ?variables ?constants
  ?constant_namespace ?namespace ?pre_process ?post_process ?process
  ?wrapper ?error ?errors ?eval_perl ?output ?output_path ?debug ?debug_format
  ?cache_size ?compile_ext ?compile_dir ?plugins ?plugin_base ?load_perl
  ?filters ?v1dollar ?load_templates ?load_plugins ?load_filters
  ?tolerant ?service ?context ?stash ?parser ?grammar () =

  let args = ref [] in
    may (fun v ->
           args := sv_of_string "START_TAG" :: sv_of_string v :: !args) start_tag;
    may (fun v ->
           args := sv_of_string "END_TAG" :: sv_of_string v :: !args) end_tag;
    may (fun v ->
           args := sv_of_string "TAG_STYLE" :: sv_of_string v :: !args) tag_style;
    may (fun v ->
           args := sv_of_string "PRE_CHOMP" :: sv_of_bool v :: !args) pre_chomp;
    may (fun v ->
           args := sv_of_string "POST_CHOMP" :: sv_of_bool v :: !args) post_chomp;
    may (fun v ->
           args := sv_of_string "TRIM" :: sv_of_bool v :: !args) trim;
    may (fun v ->
           args := sv_of_string "INTERPOLATE" :: sv_of_bool v :: !args) interpolate;
    may (fun v ->
           args := sv_of_string "ANYCASE" :: sv_of_bool v :: !args) anycase;
    may (fun v ->
           args := sv_of_string "INCLUDE_PATH" :: arrayref (av_of_string_list v) :: !args) include_path;
    may (fun v ->
           args := sv_of_string "DELIMITER" :: sv_of_string v :: !args) delimiter;
    may (fun v ->
           args := sv_of_string "ABSOLUTE" :: sv_of_bool v :: !args) absolute;
    may (fun v ->
           args := sv_of_string "RELATIVE" :: sv_of_bool v :: !args) relative;
    may (fun v ->
           args := sv_of_string "DEFAULT" :: sv_of_string v :: !args) default;
    may (fun v ->
           args := sv_of_string "BLOCKS" :: hashref (hv_of_string_pair_list v) :: !args) blocks;
    may (fun v ->
           args := sv_of_string "AUTO_RESET" :: sv_of_bool v :: !args) auto_reset;
    may (fun v ->
           args := sv_of_string "RECURSION" :: sv_of_bool v :: !args) recursion;
    may (fun v ->
           args := sv_of_string "VARIABLES" :: sv_of_variant v :: !args) variables;
    may (fun v ->
           args := sv_of_string "CONSTANTS" :: sv_of_variant v :: !args) constants;
    may (fun v ->
           args := sv_of_string "CONSTANT_NAMESPACE" :: sv_of_string v :: !args) constant_namespace;
    may (fun v ->
           args := sv_of_string "NAMESPACE" :: sv_of_variant v :: !args) namespace;
    may (fun v ->
           args := sv_of_string "PRE_PROCESS" :: arrayref (av_of_string_list v) :: !args) pre_process;
    may (fun v ->
           args := sv_of_string "POST_PROCESS" :: arrayref (av_of_string_list v) :: !args) post_process;
    may (fun v ->
           args := sv_of_string "PROCESS" :: arrayref (av_of_string_list v) :: !args) process;
    may (fun v ->
           args := sv_of_string "WRAPPER" :: arrayref (av_of_string_list v) :: !args) wrapper;
    may (fun v ->
           args := sv_of_string "ERROR" :: sv_of_string v :: !args) error;
    may (fun v ->
           args := sv_of_string "ERRORS" :: hashref (hv_of_string_pair_list v) :: !args) errors;
    may (fun v ->
           args := sv_of_string "EVAL_PERL" :: sv_of_bool v :: !args) eval_perl;
    may (fun v ->
           raise Not_implemented) output;
    may (fun v ->
           raise Not_implemented) output_path;
    may (fun v ->
           args := sv_of_string "DEBUG" :: sv_of_string v :: !args) debug;
    may (fun v ->
           args := sv_of_string "DEBUG_FORMAT" :: sv_of_string v :: !args) debug_format;
    may (fun v ->
           args := sv_of_string "CACHE_SIZE" :: sv_of_int v :: !args) cache_size;
    may (fun v ->
           args := sv_of_string "COMPILE_EXT" :: sv_of_string v :: !args) compile_ext;
    may (fun v ->
           args := sv_of_string "COMPILE_DIR" :: sv_of_string v :: !args) compile_dir;
    may (fun v ->
           args := sv_of_string "PLUGINS" :: hashref (hv_of_string_pair_list v) :: !args) plugins;
    may (fun v ->
           args := sv_of_string "PLUGIN_BASE" :: arrayref (av_of_string_list v) :: !args) plugin_base;
    may (fun v ->
           args := sv_of_string "LOAD_PERL" :: sv_of_bool v :: !args) load_perl;
    may (fun v ->
           raise Not_implemented) filters;
    may (fun v ->
           args := sv_of_string "V1DOLLAR" :: sv_of_bool v :: !args) v1dollar;
    may (fun v ->
           raise Not_implemented) load_templates;
    may (fun v ->
           raise Not_implemented) load_plugins;
    may (fun v ->
           raise Not_implemented) load_filters;
    may (fun v ->
           args := sv_of_string "TOLERANT" :: sv_of_bool v :: !args) tolerant;
    may (fun v ->
           raise Not_implemented) service;
    may (fun v ->
           raise Not_implemented) context;
    may (fun v ->
           raise Not_implemented) stash;
    may (fun v ->
           raise Not_implemented) parser;
    may (fun v ->
           raise Not_implemented) grammar;
    let sv = call_class_method "Template" "new" !args in
      new template sv
