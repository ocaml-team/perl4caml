(** Wrapper around Perl [Net::Google::Cache] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_Net_Google_Cache.ml,v 1.4 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

let _ = eval "use Net::Google::Cache"

class net_google_cache sv =

object (self)

  method key =
    string_of_sv (call_method sv "key" [])
  method set_key v =
    call_method_void sv "key" [sv_of_string v]
  method url =
    string_of_sv (call_method sv "url" [])
  method set_url v =
    call_method_void sv "url" [sv_of_string v]
  method get =
    let sv = call_method sv "get" [] in
    if sv_is_undef sv then raise Not_found;
    string_of_sv sv

end

(* let new_ = ... *)
