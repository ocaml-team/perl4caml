(** Wrapper around Perl [HTML::Form] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_HTML_Form.ml,v 1.4 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

open Pl_HTTP_Response

let _ = eval "use HTML::Form"

class html_form (sv : sv) =

object (self)
  method sv = sv

  method attr name =
    let sv = call_method sv "attr" [sv_of_string name] in
    (* sv_is_undef doesn't work properly XXX *)
    if sv_is_undef sv then None else Some (string_of_sv sv)
  method set_attr name value =
    call_method_void sv "attr" [sv_of_string name; sv_of_string value]

  (* The following isn't a real method.  The return type of HTML::Form->inputs
   * isn't documented, but I wanted a list of input names (as strings) anyway.
   *)
  method input_names =
    let inputs = call_method_array sv "inputs" [] in
    let names = List.map (fun sv -> call_method sv "name" []) inputs in
    List.map string_of_sv names

end

let parse_document html_document base_uri =
  let svlist = call_class_method_array "HTML::Form" "parse"
		 [sv_of_string html_document; sv_of_string base_uri] in
  List.map (new html_form) svlist

let parse_response (res : http_response) =
  let svlist = call_class_method_array "HTML::Form" "parse" [res#sv] in
  List.map (new html_form) svlist
