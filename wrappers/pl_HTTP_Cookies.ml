(** Wrapper around Perl [HTTP::Cookies] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_HTTP_Cookies.ml,v 1.2 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

let _ = eval "use HTTP::Cookies"

class http_cookies sv =

object (self)
  method sv = sv

  method save ?filename () =
    let args = match filename with
	None -> []
      | Some filename -> [sv_of_string filename] in
    call_method_void sv "save" args

  method load ?filename () =
    let args = match filename with
	None -> []
      | Some filename -> [sv_of_string filename] in
    call_method_void sv "load" args

  method revert () =
    call_method_void sv "revert" []

  method as_string ?skip_discardables () =
    let args = match skip_discardables with
	None -> []
      | Some b -> [sv_of_bool b] in
    string_of_sv (call_method sv "as_string" args)

end

let new_ ?file ?autosave ?ignore_discard ?hide_cookie2 () =
  let args = ref [] in
  let may f = function None -> () | Some v -> f v in
  may (fun v ->
	 args := sv_of_string "file" :: sv_of_string v :: !args) file;
  may (fun v ->
	 args := sv_of_string "autosave" :: sv_of_bool v :: !args) autosave;
  may (fun v ->
	 args := sv_of_string "ignore_discard" :: sv_of_bool v :: !args)
    ignore_discard;
  may (fun v ->
	 args := sv_of_string "hide_cookie2" :: sv_of_bool v :: !args)
    hide_cookie2;
  let sv = call_class_method "HTTP::Cookies" "new" !args in
  new http_cookies sv
