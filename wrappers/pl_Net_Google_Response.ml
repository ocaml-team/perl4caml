(** Wrapper around Perl [Net::Google::Reponse] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_Net_Google_Response.ml,v 1.4 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

let _ = eval "use Net::Google::Response"

class net_google_response sv =

object (self)

  method documentFiltering =
    bool_of_sv (call_method sv "documentFiltering" [])
  method searchComments =
    string_of_sv (call_method sv "searchComments" [])
  method estimateTotalResultsNumber =
    int_of_sv (call_method sv "estimateTotalResultsNumber" [])
  method estimateIsExact =
    bool_of_sv (call_method sv "estimateIsExact" [])
  method searchQuery =
    string_of_sv (call_method sv "searchQuery" [])
  method startIndex =
    int_of_sv (call_method sv "startIndex" [])
  method endIndex =
    int_of_sv (call_method sv "endIndex" [])
  method searchTips =
    string_of_sv (call_method sv "searchTips" [])
  method directoryCategories =
    let sv = call_method sv "directoryCategories" [] in
    let av = deref_array sv in
    av_map (fun sv -> new net_google_response sv) av
  method searchTime =
    float_of_sv (call_method sv "searchTime" [])
  method toString =
    string_of_sv (call_method sv "toString" [])
  method title =
    string_of_sv (call_method sv "title" [])
  method url =
    string_of_sv (call_method sv "URL" [])
  method snippet =
    string_of_sv (call_method sv "snippet" [])
  method cachedSize =
    string_of_sv (call_method sv "cachedSize" [])
  method directoryTitle =
    string_of_sv (call_method sv "directoryTitle" [])
  method summary =
    string_of_sv (call_method sv "summary" [])
  method hostName =
    string_of_sv (call_method sv "hostName" [])

  (* method directoryCategory *)

end

(* let new_ = ... *)
