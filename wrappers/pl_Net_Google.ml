(** Wrapper around Perl [Net::Google] class. *)
(*  Copyright (C) 2003 Merjis Ltd.

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this library; see the file COPYING.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

    $Id: pl_Net_Google.ml,v 1.4 2008-03-01 13:02:21 rich Exp $
  *)

open Perl

open Pl_Net_Google_Cache
open Pl_Net_Google_Search
open Pl_Net_Google_Spelling

let _ = eval "use Net::Google"

let may f = function None -> () | Some v -> f v

class net_google sv =

object (self)

  method search ?key ?starts_at ?max_results ?lr ?ie ?oe ?safe ?filter () =
    let args = ref [] in
    may (fun v ->
	   args := sv_of_string "key" :: sv_of_string v :: !args) key;
    may (fun v ->
	   args := sv_of_string "starts_at" :: sv_of_int v :: !args) starts_at;
    may (fun v ->
	   args := sv_of_string "max_results" :: sv_of_int v :: !args)
      max_results;
    may (fun v ->
	   args := sv_of_string "lr" :: sv_of_string v :: !args) lr;
    may (fun v ->
	   args := sv_of_string "ie" :: sv_of_string v :: !args) ie;
    may (fun v ->
	   args := sv_of_string "oe" :: sv_of_string v :: !args) oe;
    may (fun v ->
	   args := sv_of_string "safe" :: sv_of_bool v :: !args) safe;
    may (fun v ->
	   args := sv_of_string "filter" :: sv_of_bool v :: !args) filter;
    let sv = call_method sv "search" !args in
    new net_google_search sv

  method spelling ?key ?phrase ?debug () =
    let args = ref [] in
    may (fun v ->
	   args := sv_of_string "key" :: sv_of_string v :: !args) key;
    may (fun v ->
	   args := sv_of_string "phrase" :: sv_of_string v :: !args) phrase;
    may (fun v ->
	   args := sv_of_string "debug" :: sv_of_int v :: !args) debug;
    let sv = call_method sv "spelling" !args in
    new net_google_spelling sv

  method cache ?key ?url ?debug () =
    let args = ref [] in
    may (fun v ->
	   args := sv_of_string "key" :: sv_of_string v :: !args) key;
    may (fun v ->
	   args := sv_of_string "url" :: sv_of_string v :: !args) url;
    may (fun v ->
	   args := sv_of_string "debug" :: sv_of_int v :: !args) debug;
    let sv = call_method sv "cache" !args in
    new net_google_cache sv

end

let new_ ?key ?debug () =
  let args = ref [] in
  may (fun v ->
	 args := sv_of_string "key" :: sv_of_string v :: !args) key;
  may (fun v ->
	 args := sv_of_string "debug" :: sv_of_int v :: !args) debug;
  let sv = call_class_method "Net::Google" "new" !args in
  new net_google sv
