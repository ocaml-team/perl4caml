use lib "examples";
use TestClass;

print "this is loading the 'test.pl' script!\n";

sub return_one
  {
    print "this is the 'return_one' function!\n";
    1
  }

sub return_array
  {
    print "this is the 'return_array' function!\n";
    (1, 2, 3)
  }

sub return_closure
  {
    sub { $_[0] * $_[1] }
  }

sub dies
  {
    print "this is the 'dies' function! about to die now ...\n";
    die "this is the exception message from 'dies'";
  }

sub adder
  {
    $_[0] + $_[1]
  }
