package TestClass;

sub new
  {
    my $class = shift;
    my $self = { foo => 1 };
    bless $self, $class;
  }

sub get_foo
  {
    my $self = shift;
    $self->{foo}
  }

sub set_foo
  {
    my $self = shift;
    my $value = shift;
    $self->{foo} = $value
  }

1;
